//
//  Tappable.swift
//  Towers
//
//  Created by Mikey Salinas on 10/16/15.
//  Copyright © 2015 Play Indoors. All rights reserved.
//

import Foundation
import UIKit

protocol Tappable {
    func doTheThing(touch: UITouch)
}