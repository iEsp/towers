//
//  MovementComponent.swift
//  Towers
//
//  Created by Emilio Espinosa on 9/3/15.
//  Copyright © 2015 Play Indoors. All rights reserved.
//

import SpriteKit
import GameplayKit

class MovementComponent: GKComponent {
    
    var pathToFollow: [PathNode]

    
    init(withPath newPath: [PathNode], withAgent agent: InvaderAgent) {
        pathToFollow = newPath
        
        super.init()
        
        let graphNodes: [GKGraphNode2D] = {
            var graphArray = [GKGraphNode2D]()
            for node in newPath {
                let graphNode = GKGraphNode2D(node)
                graphArray.append(graphNode)
            }
            return graphArray
        }()
        
        let path = GKPath(graphNodes: graphNodes, radius: 1)
        
        let moveGoal = GKGoal(toFollowPath: path, maxPredictionTime: 1, forward: true)
        let stayGoal = GKGoal(toStayOnPath: path, maxPredictionTime: 2)
        let speedGoal = GKGoal(toReachTargetSpeed: 100)
        let behavior = GKBehavior(goals: [moveGoal, speedGoal, stayGoal] , andWeights: [5, 1, 2])

        agent.behavior = behavior
    }
    
}