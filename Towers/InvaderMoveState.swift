//
//  InvaderMoveState.swift
//  Towers
//
//  Created by Emilio Espinosa on 9/3/15.
//  Copyright © 2015 Play Indoors. All rights reserved.
//

import SpriteKit
import GameplayKit

class InvaderMoveState: GKState {
    
    unowned var entity: Invader
    
    var elapsedTime: NSTimeInterval = 0.0
    var timeSinceBehaviorUpdate: NSTimeInterval = 0.0
    
    var path =  [GKGraphNode2D]()
    
    required init(withEntity ent: Invader, andPath: [GKGraphNode2D]) {
        entity = ent
        path = andPath
    }
    
    override func updateWithDeltaTime(seconds: NSTimeInterval) {
        
        super.updateWithDeltaTime(seconds)
        
        let path1 = GKPath(graphNodes: path, radius: 2.0)
        
        let moveGoal = GKGoal(toFollowPath: path1, maxPredictionTime: 1.0, forward: true)
        let stayGoal = GKGoal(toStayOnPath: path1, maxPredictionTime: 1.0)
        let behavior = GKBehavior(goals: [moveGoal, stayGoal] , andWeights: [5.0, 10.0])
        
        entity.agent.behavior = behavior
    }
    
}