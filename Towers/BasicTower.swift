//
//  BasicTower.swift
//  Towers
//
//  Created by Emilio Espinosa on 9/2/15.
//  Copyright © 2015 Play Indoors. All rights reserved.
//

import SpriteKit
import GameplayKit

class BasicTower: Tower {
    
    override class func loadSkin() {
        skin = SKTexture(imageNamed: "Basic Tower")
    }
    
}