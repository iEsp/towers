//
//  RenderComponent.swift
//  Towers
//
//  Created by Emilio Espinosa on 9/2/15.
//  Copyright © 2015 Play Indoors. All rights reserved.
//

import SpriteKit
import GameplayKit

struct BitMask: OptionSetType {
    
    let rawValue: UInt32
    init(rawValue: UInt32) { self.rawValue = rawValue }
    
    static let Tower       = BitMask(rawValue: 1 << 0)
    static let Invader     = BitMask(rawValue: 1 << 1)
    static let Projectile  = BitMask(rawValue: 1 << 2)
    static let WayPoint    = BitMask(rawValue: 1 << 3)
    
    var category: UInt32 {
        return rawValue
    }
    
    var collision: UInt32 {
        return 0
    }
    
    var contact: UInt32 {
        switch self.rawValue {
            
        case BitMask.Tower.rawValue:
            return BitMask.Invader.rawValue
            
        case BitMask.Invader.rawValue:
            return BitMask.Projectile.rawValue
            
        case BitMask.Projectile.rawValue:
            return 0
            
        case BitMask.WayPoint.rawValue:
            return BitMask.Invader.rawValue
            
        default:
            return 0
        }
    }
}

class RenderComponent: GKComponent {
    
    var spriteNode = EntityNode()
    
    init(withTexture texture: SKTexture?, andPhysicsBody physicsBody: SKPhysicsBody, ofType type: BitMask) {
        super.init()
        
        if let skin = texture {
            spriteNode = EntityNode(texture: skin)
            spriteNode.physicsBody = physicsBody
            spriteNode.physicsBody?.categoryBitMask = type.category
            spriteNode.physicsBody?.collisionBitMask = type.collision
            spriteNode.physicsBody?.contactTestBitMask = type.contact
        }
    }
    
    init(withAnimations:[String: [SKTexture]]) {
        
    }
    
    func getRotationToPoint(point: CGPoint) -> Float {
        
        let start = spriteNode.position
        
        let vectorPoint = CGPoint(x: point.x - start.x, y: point.y - start.y)
        let length = hypot(start.x - point.x, start.y - point.y)
        
        let norm = CGPoint(x: vectorPoint.x / length, y: vectorPoint.y / length)
        let rotation = CGFloat(2 * M_PI) - atan2(norm.x, norm.y) + CGFloat(M_PI / 2)
        
        return Float(rotation)
    }
}

class EntityNode: SKSpriteNode {
    weak var entity: GKEntity!
}