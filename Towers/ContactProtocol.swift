//
//  PhysicsComponent.swift
//  Towers
//
//  Created by Emilio Espinosa on 9/10/15.
//  Copyright © 2015 Play Indoors. All rights reserved.
//

import SpriteKit
import GameplayKit

enum ContactTime {
    case Start
    case End
}

protocol ContactEntity {
    func contactWithEntity(entity: GKEntity, atTime: ContactTime)
}
