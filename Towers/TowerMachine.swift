//
//  TowerMachine.swift
//  Towers
//
//  Created by Emilio Espinosa on 9/10/15.
//  Copyright © 2015 Play Indoors. All rights reserved.
//

import SpriteKit
import GameplayKit

class TowerMachine: GKComponent {
    
    let stateMachine: GKStateMachine
    let initialState: AnyClass
    
    init(states: [GKState]) {
        stateMachine = GKStateMachine(states: states)
        initialState = states.first!.dynamicType
    }
    
    override func updateWithDeltaTime(seconds: NSTimeInterval) {
        super.updateWithDeltaTime(seconds)
        
        stateMachine.updateWithDeltaTime(seconds)
    }
    
    func enterInitialState() {
        stateMachine.enterState(initialState)
    }
}