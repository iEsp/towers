//
//  TowerIdleState.swift
//  Towers
//
//  Created by Emilio Espinosa on 9/10/15.
//  Copyright © 2015 Play Indoors. All rights reserved.
//

import SpriteKit
import GameplayKit

class TowerIdleState: GKState {
    
    unowned var entity: Tower
    
    required init(withEntity ent: Tower) {
        entity = ent
    }
    
}