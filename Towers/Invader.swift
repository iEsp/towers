//
//  BaseInvader.swift
//  Towers
//
//  Created by Emilio Espinosa on 9/2/15.
//  Copyright © 2015 Play Indoors. All rights reserved.
//

import SpriteKit
import GameplayKit

class InvaderAgent: GKAgent2D {}

class Invader: GKEntity, GKAgentDelegate, ContactEntity {
    
    static var skin: SKTexture?
    let agent: InvaderAgent = InvaderAgent()
    var health: Int = 10
    
    var renderComponent: RenderComponent {
        let renderComponent = componentForClass(RenderComponent.self)
        return renderComponent!
    }
    
    var movementComponent: MovementComponent {
        let movementComponent = componentForClass(MovementComponent.self)
        return movementComponent!
    }
    
    init(withPath path: [PathNode]) {
        super.init()
        
        let renderComponent = RenderComponent(withTexture: Invader.skin, andPhysicsBody: SKPhysicsBody(rectangleOfSize: Invader.skin!.size()), ofType: .Invader)
        addComponent(renderComponent)
        renderComponent.spriteNode.entity = self
        renderComponent.spriteNode.position = path.first!.node.position
        
        agent.rotation = renderComponent.getRotationToPoint(path[1].node.position)
        agent.maxSpeed = 100
        agent.maxAcceleration = 100
        agent.radius = 0.2
        agent.delegate = self
        addComponent(agent)
        
        let movementComponent = MovementComponent(withPath: path, withAgent: agent)
        addComponent(movementComponent)
    }
    
    func contactWithEntity(entity: GKEntity, atTime: ContactTime) {
        if entity.isKindOfClass(Tower) {
            return
        }
    }
    
    func agentWillUpdate(agent: GKAgent) {
        updateAgentPositionToNode()
    }
    
    func agentDidUpdate(agent: GKAgent) {
        updateSpritePositionToAgent()
    }
    
    private func updateSpritePositionToAgent() {
        let agentPos = CGPoint(x: CGFloat(agent.position.x), y: CGFloat(agent.position.y))
        renderComponent.spriteNode.position = CGPoint(x: CGFloat(agentPos.x), y: CGFloat(agentPos.y))
    }
    
    private func updateAgentPositionToNode() {
        let renderComp = self.renderComponent
        agent.position = float2(Float(renderComp.spriteNode.position.x), Float(renderComp.spriteNode.position.y))
    }
    
    class func loadSkin() {}
}