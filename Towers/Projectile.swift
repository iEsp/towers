//
//  Projectile.swift
//  Towers
//
//  Created by Emilio Espinosa on 9/10/15.
//  Copyright © 2015 Play Indoors. All rights reserved.
//

import SpriteKit
import GameplayKit

class Projectile: GKEntity, ContactEntity {
    static var skin: SKTexture?
    
    var target: InvaderAgent?
    var damage: Int = 5
    var lastTargetPos: CGPoint!
    var speed: CGFloat = 500
    
    var renderComponent: RenderComponent {
        let renderComponent = componentForClass(RenderComponent.self)
        return renderComponent!
    }
    
    init(withTarget invader: InvaderAgent) {
        
        super.init()
        
        let renderComponent = RenderComponent(withTexture: Projectile.skin, andPhysicsBody: SKPhysicsBody(circleOfRadius: Projectile.skin!.size().width / 2), ofType: BitMask.Projectile)
        addComponent(renderComponent)
        
        target = invader
    }
    
    func calcTargetLocation() {
        let node = renderComponent.spriteNode
        let targetPos = CGPoint(target!.position)
        lastTargetPos = targetPos

        let offset = CGPointMake(targetPos.x - node.position.x, targetPos.y - node.position.y)
        let length = sqrt((offset.x * offset.x) + (offset.y * offset.y))
        let direction = CGPointMake(offset.x / length, offset.y / length)
        
        let velocity = CGVectorMake(direction.x * speed, direction.y * speed)
        node.physicsBody!.velocity = velocity
    }
    
    func toLastPos() {
        let node = renderComponent.spriteNode
        node.physicsBody!.velocity = CGVectorMake(0, 0)
        
        let offset = CGPointMake(lastTargetPos.x - node.position.x, lastTargetPos.y - node.position.y)
        let length = sqrt((offset.x * offset.x) + (offset.y * offset.y))
        
        let moveAction = SKAction.moveTo(lastTargetPos, duration: NSTimeInterval(length / speed))
        let removeAction = SKAction.removeFromParent()
        let sequence = SKAction.sequence([moveAction, removeAction])
        
        node.runAction(sequence)
    }
    
    func contactWithEntity(entity: GKEntity, atTime: ContactTime) {
        let scene = renderComponent.spriteNode.scene as! GameScene
        
        let enemy = entity as! Invader
        enemy.health -= damage
        print(enemy.health)
        
        renderComponent.spriteNode.removeFromParent()
        scene.projectiles.remove(self)
    }
    
    override func updateWithDeltaTime(seconds: NSTimeInterval) {
        if target != nil {
            calcTargetLocation()
        } else {
            toLastPos()
        }
    }
    
    static func loadSkin() {
        skin = SKTexture(imageNamed: "Rock")
    }
}