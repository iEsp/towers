//
//  BaseTower.swift
//  Towers
//
//  Created by Emilio Espinosa on 9/2/15.
//  Copyright © 2015 Play Indoors. All rights reserved.
//

import SpriteKit
import GameplayKit

class Tower: GKEntity, Tappable {
    
    static var skin: SKTexture?
    static var targetingNodes = Set<PathNode>()
    
    var targetRadius: CGFloat = 200
    var targetRegion = SKRegion(radius: 200)
    var targetNodes = [PathNode]()
    var delegate: MenuDelegate?
    var menuShowing = false
    
    // TODO: Should we do this instead?
    //var position: CGPoint = CGPoint(x: 0, y: 0)
    
    var renderComponent: RenderComponent {
        let renderComponent = componentForClass(RenderComponent.self)
        return renderComponent!
    }
    
    var targetingComponent: TargetingComponent {
        let targetingComponent = componentForClass(TargetingComponent.self)
        return targetingComponent!
    }
    
    override init() {
        super.init()
        
        let renderComponent = RenderComponent(withTexture: Tower.skin, andPhysicsBody: SKPhysicsBody(circleOfRadius: targetRadius), ofType: .Tower)
        addComponent(renderComponent)
        renderComponent.spriteNode.name = "tower"
        renderComponent.spriteNode.entity = self
        
        let targetComponent = TargetingComponent()
        addComponent(targetComponent)
        
        let shootingComponent = ShootingComponent()
        addComponent(shootingComponent)
        
    }
    
    class func loadSkin() {}
    
    // protocol method
    func doTheThing(touch: UITouch) {
        let location = touch.locationInNode(renderComponent.spriteNode)
        let sprite = renderComponent.spriteNode.nodeAtPoint(location)
        // TODO: convert into switch statement or pass string into function as arg
        if sprite.name == "tower" {
            if !menuShowing {
                showMenu()
                menuShowing = true
            }
            else {
                // remove menu
                menuShowing = false
            }
            
        }
        print(sprite.name)
    }
    
    func showMenu(){
        let item1 = SKSpriteNode(color: SKColor.blueColor(), size: CGSize(width: 20, height: 20))
        item1.position = CGPoint(x: -10, y: 50)
        item1.name = "1"
        renderComponent.spriteNode.addChild(item1)
    }
}

class PathNode: GKEntity, ContactEntity {
    
    var queue = [Invader]()
    var node = EntityNode()
    
    var sectionNumber = 0
    var distanceToEnd: CGFloat = 0
    
    override init() {
        super.init()
        node.physicsBody = SKPhysicsBody(circleOfRadius: 15.0)
        node.physicsBody?.categoryBitMask = BitMask.WayPoint.category
        node.physicsBody?.contactTestBitMask = BitMask.WayPoint.contact
        node.physicsBody?.collisionBitMask = BitMask.WayPoint.collision
        node.entity = self
    }
    
    func contactWithEntity(entity: GKEntity, atTime time: ContactTime) {
        if let enemy = entity as? Invader {
            if time == .Start {
                queue.append(enemy)
            } else {
                var count = 0
                for target in queue {
                    if enemy === target {
                        queue.removeAtIndex(count)
                        break
                    }
                    count++
                }
            }
        }
    }
}