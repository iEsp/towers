//
//  Extensions.swift
//  Towers
//
//  Created by Emilio Espinosa on 9/9/15.
//  Copyright © 2015 Play Indoors. All rights reserved.
//

import SpriteKit
import GameplayKit

extension CGPoint {
    init(_ point: float2) {
        x = CGFloat(point.x)
        y = CGFloat(point.y)
    }
}

extension float2 {
    init(_ point: CGPoint) {
        self.init(x: Float(point.x), y: Float(point.y))
    }
}

extension String {
    func contains(find: String) -> Bool{
        return self.rangeOfString(find) != nil
    }
}

extension GKGraphNode2D {
    convenience init(_ node: PathNode) {
        self.init()
        self.position = vector_float2(x: Float(node.node.position.x), y: Float(node.node.position.y))
    }
}