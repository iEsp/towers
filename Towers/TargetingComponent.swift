//
//  ShootingComponent.swift
//  Towers
//
//  Created by Emilio Espinosa on 9/10/15.
//  Copyright © 2015 Play Indoors. All rights reserved.
//

import SpriteKit
import GameplayKit

class TargetingComponent: GKComponent {
    
    var targetAgent: InvaderAgent?
//    var endPoint: CGPoint
//    var farPoint: CGPoint!
    
//    init(withEnd end: CGPoint) {
//        endPoint = end
//        super.init()
//    }
//    
//    func findTargetingPoint() {
//        if let tower = entity as? Tower {
//            let towerPosition = tower.renderComponent.spriteNode.position
//            
//            let directionVector = CGPoint(x: endPoint.x - towerPosition.x, y: endPoint.y - towerPosition.y)
//            let length = hypot(endPoint.x - towerPosition.x, endPoint.y - towerPosition.y)
//            let norm = CGPoint(x: directionVector.x / length, y: directionVector.y / length)
//            farPoint = CGPoint(x: towerPosition.x - (norm.x * tower.targetRadius), y: towerPosition.y - (norm.y * tower.targetRadius))
//        }
//    }
//    
//    func isEnemyCloser(enemy: Invader, target: Invader) -> Bool {
//        let length1 = hypot(farPoint.x - enemy.renderComponent.spriteNode.position.x, farPoint.y - enemy.renderComponent.spriteNode.position.y)
//        let length2 = hypot(farPoint.x - target.renderComponent.spriteNode.position.x, farPoint.y - target.renderComponent.spriteNode.position.y)
//        
//        return length1 < length2
//    }
    
    func findTargetingNodes() {
        let tower = entity as! Tower
        
        var prelimNodes = [PathNode]()
        for pathNode in Tower.targetingNodes {
            let towerPos = tower.renderComponent.spriteNode.convertPoint(tower.renderComponent.spriteNode.position, toNode: tower.renderComponent.spriteNode.scene!)
            let nodePos = pathNode.node.position
            let distance = hypot(nodePos.x - towerPos.x, nodePos.y - towerPos.y)
            if distance <= tower.targetRadius {
                prelimNodes.append(pathNode)
            }
        }
        
//        let unique = Array(Set(prelimNodes))
        tower.targetNodes = prelimNodes.sort({
            switch ($0.sectionNumber,$1.sectionNumber) {
            case let (lhs,rhs) where lhs == rhs:
                return $0.distanceToEnd > $1.distanceToEnd
            case let (lhs, rhs):
                return lhs > rhs
            }
        })

    }
    
    func findTarget() -> InvaderAgent? {
        if let tower = entity as? Tower {
            for node in tower.targetNodes {
                if !node.queue.isEmpty {
                    return node.queue.first!.agent
                }
            }
        }
        return nil
    }
    
    override func updateWithDeltaTime(seconds: NSTimeInterval) {
            targetAgent = findTarget()
    }
}