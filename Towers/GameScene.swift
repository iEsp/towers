//
//  GameScene.swift
//  Towers
//
//  Created by Emilio Espinosa on 8/31/15.
//  Copyright (c) 2015 Play Indoors. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene, SKPhysicsContactDelegate {
    
    var towersNode: SKNode!
    var invadersNode: SKNode!
    
    var lastUpdateTimeInterval: NSTimeInterval = 0
    
    var entities: Set<GKEntity> = Set()
    var projectiles: Set<Projectile> = Set()
    
    let invAgentSys = GKComponentSystem(componentClass: InvaderAgent.self)
    let moveSys = GKComponentSystem(componentClass: MovementComponent.self)
    let targetSys = GKComponentSystem(componentClass: TargetingComponent.self)
    let shootingSys = GKComponentSystem(componentClass: ShootingComponent.self)
    
    var pathArray = [String: [GKGraphNode2D]]()
    var pathNodeArray = [String: [PathNode]]()
    
    override func didMoveToView(view: SKView) {
        
        towersNode = childNodeWithName("Towers")
        invadersNode = childNodeWithName("Invaders")

        loadAssets()
        setPaths()
        
        physicsWorld.gravity = CGVector(dx: 0, dy: 0)
        physicsWorld.contactDelegate = self
        
        let newInvader = Nomad(withPath: pathNodeArray["Path2"]!)
        let newInvader2 = Nomad(withPath: pathNodeArray["Path1"]!)
        entities.insert(newInvader)
        entities.insert(newInvader2)
        invAgentSys.addComponentWithEntity(newInvader)
        moveSys.addComponentWithEntity(newInvader)
        invAgentSys.addComponentWithEntity(newInvader2)
        moveSys.addComponentWithEntity(newInvader2)
        invadersNode.addChild(newInvader.renderComponent.spriteNode)
        invadersNode.addChild(newInvader2.renderComponent.spriteNode)

        for node in towersNode.children {
            let newTower = Tower()
            //newTower.delegate = self
            entities.insert(newTower)
            node.addChild(newTower.renderComponent.spriteNode)
            targetSys.addComponentWithEntity(newTower)
            shootingSys.addComponentWithEntity(newTower)
            newTower.targetingComponent.findTargetingNodes()
            
        }
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        for touch in touches {
            let location = touch.locationInNode(self)
            let nodes = self.nodesAtPoint(location)
            for node in nodes {
                if let nodeEntity = node as? EntityNode {
                    if let tappableEntity = nodeEntity.entity as? Tappable{
                        tappableEntity.doTheThing(touch)
                    }
                }
            }
        }
    }
    
    func didBeginContact(contact: SKPhysicsContact) {
        handlePhysics(contact, atTime: .Start)
    }
    
    func didEndContact(contact: SKPhysicsContact) {
        handlePhysics(contact, atTime: .End)
    }
    
    private func handlePhysics(contact: SKPhysicsContact, atTime time: ContactTime) {
        let entityA = (contact.bodyA.node as? EntityNode)?.entity
        let entityB = (contact.bodyB.node as? EntityNode)?.entity
        
        if let notifyEnt = entityA as? ContactEntity {
            notifyEnt.contactWithEntity(entityB!, atTime: time)
        }
        if let notifyEnt = entityB as? ContactEntity {
            notifyEnt.contactWithEntity(entityA!, atTime: time)
        }
    }
   
    override func update(currentTime: NSTimeInterval) {
        
        var deltaTime = currentTime - lastUpdateTimeInterval
        deltaTime = deltaTime > (1.0 / 60) ? (1.0 / 60) : deltaTime
        
        lastUpdateTimeInterval = currentTime
        for proj in projectiles {
            proj.updateWithDeltaTime(deltaTime)
        }
        
        invAgentSys.updateWithDeltaTime(deltaTime)
        moveSys.updateWithDeltaTime(deltaTime)
        targetSys.updateWithDeltaTime(deltaTime)
        shootingSys.updateWithDeltaTime(deltaTime)
    }
    
    func loadAssets() {
        BasicTower.loadSkin()
        Nomad.loadSkin()
        Projectile.loadSkin()
    }
    
    func showMenu(ent: Tower){

    }
    
    // Read path nodes and add them to their corresponding dictionary key
    func setPaths() {
        if let pathNode = childNodeWithName("Paths") {
            
            for path in pathNode.children {
                
                if path.name!.containsString("CommonPath") {
                    
                    var tempArray = [PathNode]()
                    let section = Int(path.name!.substringFromIndex(path.name!.endIndex.advancedBy(-2)))
                    
                    var distance: CGFloat = 0
                    for i in 0..<(path.children.count - 1) {
                        distance += CGFloat(hypot(path.children[i + 1].position.x - path.children[i].position.x, path.children[i + 1].position.y - path.children[i].position.y))
                    }
                    
                    var lastNodePos = path.children.first!.position
                    var travelled: CGFloat = 0.0
                    for node in path.children {
                        let newPathNode = PathNode()
                        newPathNode.sectionNumber = section!
                        newPathNode.node.position = node.position
                        travelled += hypot(lastNodePos.x - newPathNode.node.position.x, lastNodePos.y - newPathNode.node.position.y)
                        
                        newPathNode.distanceToEnd = distance - travelled
                        
                        lastNodePos = newPathNode.node.position
                        tempArray.append(newPathNode)
                    }
                    
                    for (key, _) in pathNodeArray {
                        for element in tempArray {
                            pathNodeArray[key]?.append(element)

                        }
                    }
                    
//                    for (key, _) in pathArray {
//                        
//                        
//                        
//                        for node in path.children {
//                            pathArray[key]?.append(GKGraphNode2D(point: float2(node.position)))
//                            let newNode = PathNode()
//                            newNode.node.position = node.position
//                            entities.insert(newNode)
//                            addChild(newNode.node)
//                        }
//                    }
                    
                } else {
                    let pathName = path.name!.substringToIndex(path.name!.endIndex.advancedBy(-3))
                    let section = Int(path.name!.substringFromIndex(path.name!.endIndex.advancedBy(-2)))
                    
//                    pathArray[pathName] = []
                    pathNodeArray[pathName] = []
                    
                    var distance: CGFloat = 0
                    for i in 0..<(path.children.count - 1) {
                        distance += CGFloat(hypot(path.children[i + 1].position.x - path.children[i].position.x, path.children[i + 1].position.y - path.children[i].position.y))
                    }
                    
                    var lastNodePos = path.children.first!.position
                    var travelled: CGFloat = 0.0
                    for node in path.children {
                        let newPathNode = PathNode()
                        newPathNode.sectionNumber = section!
                        newPathNode.node.position = node.position
                        travelled += hypot(lastNodePos.x - newPathNode.node.position.x, lastNodePos.y - newPathNode.node.position.y)
                        
                        newPathNode.distanceToEnd = distance - travelled
                        
                        lastNodePos = newPathNode.node.position
                        pathNodeArray[pathName]?.append(newPathNode)
                    }
                    
//                    for node in path.children {
//                        pathArray[path.name!]?.append(GKGraphNode2D(point: float2(node.position)))
//                    }
                }
            }
            
            pathNode.removeFromParent()
        }
        
        var pathSet = Set<PathNode>()
        for (_, nodeArray) in pathNodeArray {
            pathSet = pathSet.union(Set(nodeArray))
        }
        
        for node in pathSet {
            entities.insert(node)
            addChild(node.node)
        }
        
//        var targetingNodes = [String: [PathNode]]()
//        
//        for (key, _) in pathArray {
//            var distance: CGFloat = 0
//            for i in 0..<(pathArray[key]!.count - 1) {
//                distance += CGFloat(hypot(pathArray[key]![i + 1].position.x - pathArray[key]![i].position.x, pathArray[key]![i + 1].position.y - pathArray[key]![i].position.y))
//            }
//            
//            var travelled: CGFloat = 0
//            var lastNodePos: CGPoint = CGPoint((pathArray[key]!.first! as GKGraphNode2D).position)
//            for node in 0..<pathArray[key]!.count {
//                let newNode = PathNode()
//                newNode.node.position = CGPoint(pathArray[key]![node].position)
//                
//                travelled += CGFloat(hypot(lastNodePos.x - newNode.node.position.x, lastNodePos.y - newNode.node.position.y))
////                newNode.progressDownPath = 1 - ((distance - travelled) / distance)
//                lastNodePos = newNode.node.position
//                
//                entities.insert(newNode)
//                addChild(newNode.node)
//                if targetingNodes[key] == nil {
//                    targetingNodes[key] = []
//                }
//                targetingNodes[key]?.append(newNode)
//            }
//        }
        
        Tower.targetingNodes = pathSet
    }
}
