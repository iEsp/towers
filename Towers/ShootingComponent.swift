//
//  ShootingComponent.swift
//  Towers
//
//  Created by Emilio Espinosa on 9/17/15.
//  Copyright © 2015 Play Indoors. All rights reserved.
//

import SpriteKit
import GameplayKit

class ShootingComponent: GKComponent {
    
    var rateOfFire = 1.0
    var lastTimeSinceFire: NSTimeInterval = 0.0
    
    override func updateWithDeltaTime(seconds: NSTimeInterval) {
        if let target = (entity as! Tower).targetingComponent.targetAgent where (lastTimeSinceFire >= rateOfFire) {
            let newProjectile = Projectile(withTarget: target)
            newProjectile.renderComponent.spriteNode.position = (entity as! Tower).renderComponent.spriteNode.position
            let scene = (entity as! Tower).renderComponent.spriteNode.scene as! GameScene
            let startPos = (entity as! Tower).renderComponent.spriteNode.convertPoint((entity as! Tower).renderComponent.spriteNode.position, toNode: scene)
            newProjectile.renderComponent.spriteNode.position = startPos
            newProjectile.renderComponent.spriteNode.entity = newProjectile
            
            scene.invadersNode.addChild(newProjectile.renderComponent.spriteNode)
            scene.projectiles.insert(newProjectile)
            lastTimeSinceFire = 0.0
        }
        lastTimeSinceFire += seconds
    }
    
}
